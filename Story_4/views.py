from django.shortcuts import render
from .forms import formPage
from .models import myModels
from django.http import HttpResponseRedirect

def about_view(request):
    return render(request, "about.html")

def skills_view(request):
    return render(request, "skills.html")

def experience_view(request):
    return render(request, "experience.html")

def registration_view(request):
    return render(request, "registration.html")

def schedule_view(request):
    response = {'form': formPage}
    return render(request, "schedule.html", response)

def result_view(request):
    result = myModels.objects.all()
    response = {'result': result}
    return render(request, "result.html", response)

def formView(request):
    response = {}
    sched = formPage(request.POST or None)
    if (request.method == 'POST' and sched.is_valid()):
        response['date'] = request.POST['date']
        response['activity'] = request.POST['activity']
        response['location'] = request.POST['location']
        response['category'] = request.POST['category']
        schedule_form = myModels(date=response['date'],
                            activity=response['activity'],
                            location=response['location'],
                            category=response['category'])
        schedule_form.save()
        return HttpResponseRedirect('/activity/')
    else:
        return HttpResponseRedirect('/schedule/')

def delete_database(request):
    myModels.objects.all().delete()
    return HttpResponseRedirect('/activity/')
