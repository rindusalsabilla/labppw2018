from django.db import models

# Create your models here.
class myModels(models.Model):
    date = models.DateTimeField()
    activity = models.CharField(max_length=30)
    location = models.CharField(max_length=30)
    category = models.CharField(max_length=30)
